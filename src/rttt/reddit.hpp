#pragma once

#include <iostream>
#include <queue>
#include <ranges>
#include <set>
#include <string>

#include "cpr/cpr.h"
#include "nlohmann/json.hpp"

#include "ftxui/component/component.hpp"

#include "rttt.hpp"
#include "rttt/config.hpp"
#include "rttt/functional.hpp"
#include "rttt/future.hpp"
#include "rttt/logger.hpp"
#include "rttt/request.hpp"
#include "rttt/socket.hpp"
#include "rttt/storage.hpp"
#include "rttt/text.hpp"
#include "rttt/ui_ftxui.hpp"
#include "rttt/view.hpp"

namespace rttt {
namespace reddit {

bool is_valid_path(std::string_view name) {
  // TODO: Ideally we should not split it twice (here and in parse_path)
  auto v = rttt::text::split(name, "/");
  return (v.size() > 0 && v[0] == "r");
}

bool is_valid_path(const rttt::Path &path) {
  // TODO: should we rely on path.type if it is present?
  return (path.parts.size() == 2 || path.parts.size() == 4) &&
         path.parts[0] == "r";
}

struct credentials {
  cpr::Header header = cpr::Header{{"User-Agent", "rttt/1.1.1"}};
  std::string client_id;
  std::string device_id;
  std::string refresh_token;
  std::string access_token;
};

struct state {
  reddit::credentials credentials;
  future::future_wait_list<cpr::Response> wait_list;

  std::string username = {};

  std::vector<std::function<void(state &)>> on_login;
};

namespace {
request::State<std::string> requestCache;
std::string base_path = "https://oauth.reddit.com";
} // namespace

inline std::optional<std::string> try_to_extract_code(const std::string &line) {
  std::string str;
  // GET /?state=abcde&code=och5-csNgMKnOB6hLSVqOILJk9uqzw HTTP/1.1
  std::regex e("code=(\\S+) ");
  std::smatch sm;
  std::regex_search(line, sm, e);
  if (sm.size() > 1) {
    return sm[1];
  }
  return std::nullopt;
}

state retrieve_access_token(state &&state) {
  auto &credentials = state.credentials;
  auto auth = cpr::Authentication {
    credentials.client_id, ""
#if (CPR_VERSION_MAJOR > 1 || CPR_VERSION_MINOR > 8)
        ,
        cpr::AuthMode::BASIC
#endif
  };
  auto uri = cpr::Url{"https://www.reddit.com/api/v1/access_token"};
  credentials.access_token = "";

  if (credentials.refresh_token.empty()) {
    state.username = {};
    auto payload = cpr::Payload{
        {"grant_type", "https://oauth.reddit.com/grants/installed_client"},
        {"device_id", credentials.device_id}};

    state.wait_list.push_back(
        {cpr::PostAsync(uri, payload, credentials.header, auth,
                        cpr::Timeout{5000}),
         [&credentials](cpr::Response &&r) {
           if (r.status_code != 200) {
             std::cerr << "Could not obtain an access_token for reddit."
                       << std::endl;
             std::cerr << r.text << std::endl;
           }

           logger::log_ifnot(r.status_code == 200);
           nlohmann::json j = nlohmann::json::parse(r.text);
           credentials.access_token = j["access_token"];
         }});
  } else {
    auto payload = cpr::Payload{{"grant_type", "refresh_token"},
                                {"refresh_token", credentials.refresh_token}};
    state.wait_list.push_back(
        {cpr::PostAsync(uri, payload, credentials.header, auth,
                        cpr::Timeout{5000}),
         [&state, &credentials](cpr::Response &&r) {
           if (r.status_code != 200) {
             std::cerr << "Could not obtain an access_token for reddit."
                       << std::endl;
             std::cerr << r.text << std::endl;
           }

           logger::log_ifnot(r.status_code == 200);
           auto debug_old_token = credentials.refresh_token;
           auto j = nlohmann::json::parse(r.text);
           credentials.refresh_token = j["refresh_token"];

           // I think there is a chance refresh token changes so test if that
           // ever happens, if so then we will need to handle that (save it to
           // config file) We'll probably make that the responsibility of the
           // calling function
           // TODO: remove this debug_old_token and assert if this either has
           // been implemented or is not needed at all in practice
           logger::log_ifnot(credentials.refresh_token == debug_old_token);
           credentials.access_token = j["access_token"];

           // Get the username for display.
           auto url = base_path + "/api/v1/me";
           state.wait_list.push_back(
               {cpr::GetAsync(cpr::Url{url}, state.credentials.header,
                              cpr::Bearer{state.credentials.access_token},
                              cpr::Timeout{5000}),
                [&state](cpr::Response &&r) {
                  logger::log_ifnot(r.status_code == 200);
                  nlohmann::json j = nlohmann::json::parse(r.text);
                  state.username = j["name"];
                  for (auto &f : state.on_login) {
                    f(state);
                  }
                }});
         }});
  }
  return std::move(state);
}

state retrieve_refresh_token(state &&state) {
  std::string redirect_uri = "http://localhost:6501";

  // Build up url
  /*https://www.reddit.com/api/v1/authorize?client_id=CLIENT_ID&response_type=TYPE&
      state=RANDOM_STRING&redirect_uri=URI&duration=DURATION&scope=SCOPE_STRING*/
  // REQUIRE(false); // double check scopes
  std::string url = "https://www.reddit.com/api/v1/authorize?client_id=" +
                    state.credentials.client_id + "&response_type=code" +
                    "&state=" + state.credentials.device_id +
                    "&redirect_uri=" + redirect_uri + "&duration=permanent" +
                    "&scope=read mysubreddits identity vote";

  auto socket = rttt::socket::listen(6501);
  rttt::openInBrowser(url);

  auto future =
      std::async([socket, redirect_uri, credentials = state.credentials]() {
        auto reply_string = rttt::socket::read_reply_and_close(socket);
        auto maybe_code = reddit::try_to_extract_code(reply_string);

        logger::log_ifnot(maybe_code);

        // Request token as before, but with
        // grant_type=authorization_code&code=CODE&redirect_uri=URI
        auto payload = cpr::Payload{{"grant_type", "authorization_code"},
                                    {"code", maybe_code.value()},
                                    {"redirect_uri", redirect_uri}};

        // TODO: Consider moving auth to credentials
        auto auth = cpr::Authentication {
          credentials.client_id, ""
#if (CPR_VERSION_MAJOR > 1 || CPR_VERSION_MINOR > 8)
              ,
              cpr::AuthMode::BASIC
#endif
        };

        auto uri = cpr::Url{"https://www.reddit.com/api/v1/access_token"};
        return cpr::Post(uri, payload, credentials.header, auth,
                         cpr::Timeout{5000});
      });

  state.wait_list.push_back({std::move(future), [&state](cpr::Response &&r) {
                               logger::log_ifnot(r.status_code == 200);
                               nlohmann::json j = nlohmann::json::parse(r.text);
                               state.credentials.access_token =
                                   j["access_token"];
                               state.credentials.refresh_token =
                                   j["refresh_token"];
                               state = retrieve_access_token(std::move(state));
                             }});

  return std::move(state);
}

state retrieve_access_token(state &&state, const nlohmann::json &config) {
  // TODO: add a config_to_credentials and credentials_to_config,
  // (extract_credential_from_config?) then remove this wrapper function
  auto &credentials = state.credentials;
  credentials.client_id = config["reddit"]["client_id"];
  credentials.device_id = config["reddit"]["device_id"];
  credentials.refresh_token = config["reddit"]["refresh_token"];
  credentials.access_token = "";
  return retrieve_access_token(std::move(state));
}

template <typename F> state retrieve_subreddits(state &&state, F callback) {
  auto url = base_path + "/subreddits/mine/subscriber";
  auto parameters = cpr::Parameters{{"limit", "100"}};
  state.wait_list.push_back(
      {cpr::GetAsync(cpr::Url{url}, state.credentials.header,
                     cpr::Bearer{state.credentials.access_token}, parameters,
                     cpr::Timeout{5000}),

       [&callback](cpr::Response &&r) {
         logger::log_ifnot(r.status_code == 200);
         nlohmann::json j = nlohmann::json::parse(r.text);
         std::set<std::string> subs;
         subs.insert("/r/front");
         for (auto &child : j["data"]["children"]) {
           subs.insert(
               "/" + child["data"]["display_name_prefixed"].get<std::string>());
         }
         callback(subs);
       }});

  return state;
}

inline std::string getURIFromPath(const rttt::Path &path) {
  auto basename = path.basename;
  if (basename == "/r/front")
    basename = "";
  if (path.mode == rttt::list_mode::comment)
    return base_path + basename + "/comments/" + path.id;
  return base_path + basename + "/best";
}

inline void requestURI(const rttt::Path &path) {
  auto url = getURIFromPath(path);
  requestCache = request::push(std::move(requestCache), url, path.name);
}

template <typename T> T parseData(const nlohmann::json &json) {
  T data;
  data.by = json["author"];
  data.time = json["created"];
  data.id = json["id"];
  data.score = json["score"];
  data.fullname = json["name"];

  if (!json["likes"].is_null())
    data.vote = json["likes"] ? 1 : -1;

  if constexpr (std::is_same<T, reddit::Comment>::value) {
    data.text = rttt::parse_html(json["body"]);
  } else if constexpr (std::is_same<T, reddit::Story>::value) {
    data.title = rttt::parse_html(json["title"]);
    // itemdata.domain = j["data"]["subreddit_name_prefixed"];
    data.domain = json["domain"];
    data.descendants = json["num_comments"];
    data.url = json["url"];
    data.text = rttt::parse_html(json["selftext"]);
  }

  return data;
}

inline std::vector<rttt::ItemVariant> parseListing(const nlohmann::json &json) {
  std::vector<rttt::ItemVariant> result;
  for (auto &j : json["data"]["children"]) {
    auto itemdata = parseData<reddit::Story>(j["data"]);
    result.push_back(itemdata);
  }
  return result;
}

inline std::vector<rttt::ItemVariant>
parseChildren(const nlohmann::json &json) {
  std::vector<rttt::ItemVariant> result;
  for (auto &j : json) {
    if (!j["data"].contains("created"))
      continue;

    reddit::Comment comment = parseData<reddit::Comment>(j["data"]);

    if (j["data"].contains("replies") && j["data"]["replies"] != "")
      comment.kids = parseChildren(j["data"]["replies"]["data"]["children"]);

    result.push_back(comment);
  }
  return result;
}

inline std::vector<rttt::ItemVariant>
parseComments(const nlohmann::json &json) {
  std::vector<rttt::ItemVariant> result;
  // The story:
  auto &j = json[0]["data"]["children"][0]["data"];

  reddit::Story itemdata = parseData<reddit::Story>(j);

  if (json.size() > 1)
    itemdata.kids = parseChildren(json[1]["data"]["children"]);

  result.push_back(itemdata);

  // Children
  return result;
}

namespace {
// Note that as long as items are in the pathCache (main) they will keep being
// marked active and we probably don't need to live long after that (15mins
// currently, i.e. 900 seconds)
rttt::active_storage<std::string, std::vector<rttt::ItemVariant>> items =
    rttt::active_storage<std::string, std::vector<rttt::ItemVariant>>(
        300, 10, 1000,
        [](const std::string &key,
           [[maybe_unused]] const std::vector<rttt::ItemVariant> &value) {
          requestURI(rttt::parse_path(key));
        });

} // namespace

bool update(state &state) {
  auto &credentials = state.credentials;
  future::update(state.wait_list);

  if (credentials.access_token.empty())
    return false;

  auto parameters = cpr::Parameters{{"limit", "100"}};

  auto request_function = [&parameters, &credentials](std::string url) {
    return cpr::GetAsync(cpr::Url{url}, credentials.header,
                         cpr::Bearer{credentials.access_token}, parameters,
                         cpr::Timeout{5000});
  };

  auto error_handling = [&state](int status_code) {
    if (status_code == 401) {
      auto config = config::load();
      state = retrieve_access_token(std::move(state), config);
      logger::push("Reddit: requesting new access token");
      return true;
    }
    return false;
  };

  requestCache = request::update(std::move(requestCache), request_function,
                                 error_handling);

  bool updated = false;
  items.update();

  auto received = request::try_pop_and_retrieve(requestCache);
  while (received.has_value()) {
    auto &key = received.value().first;
    auto json = nlohmann::json::parse(received.value().second);
    // Pretty print json
    // std::cout << json.dump(2) << std::endl;

    if (json.is_array()) {
      if (items.contains(key)) {
        items.at(key) = parseComments(json);
        items.mark_updated(key);
      } else {
        items.insert({key, parseComments(json)});
      }
    } else {
      if (items.contains(key)) {
        items.at(key) = parseListing(json);
        items.mark_updated(key);
      } else {
        items.insert({key, parseListing(json)});
      }
    }
    items.mark_updated(key);
    updated = true;
    received = request::try_pop_and_retrieve(requestCache);
  }
  return updated;
}

inline rttt::Path updatePath(rttt::Path &&path) {
  logger::log_ifnot(reddit::is_valid_path(path));
  if (!items.contains(path.name)) {
    items.insert({path.name, {}});
  }
  items.mark_active(path.name);
  return std::move(path);
}

template <typename T> auto get_stories(T &&items, const rttt::Path &path) {
  typename T::mapped_value vec;
  items.mark_active(path.name);
  auto v = items.at(path.name) | std::views::transform([](const auto &item) {
             return std::pair(0, item);
           });
  return std::pair(std::move(items), v);
}

auto create_login_popup(const state &state, ui::state &ui_state) {
  ui::popup_state popup_state;

  popup_state.title = "Reddit login";
  popup_state.content = ftxui::vbox(
      {ftxui::separatorEmpty(),
       ftxui::paragraph("Please check the newly opened page in your browser "
                        "and authorize rttt's Reddit access"),
       ftxui::separatorEmpty()});

  popup_state.event_handler = [&state,
                               &ui_state]([[maybe_unused]] ftxui::Event event) {
    if (!state.credentials.refresh_token.empty()) {
      ui_state.popup = std::nullopt;
      auto cfg = config::load();
      cfg["reddit"]["refresh_token"] = state.credentials.refresh_token;
      config::save(cfg);
    }
    return false;
  };
  return popup_state;
}

/*
 * Implementing the new thing API
 */

bool catch_ui_event(state &state, ui::state &ui_state, const rttt::Path &path,
                    const ItemVariant &item, ftxui::Event event) {
  if (event == ftxui::Event::Character('a') ||
      event == ftxui::Event::Character('z')) {

    return rttt::try_vote(item) | try_with([&event](auto vote) {
             if (event == ftxui::Event::Character('a')) {
               if (vote != 1)
                 return "1";
             } else if (event == ftxui::Event::Character('z')) {
               if (vote != -1)
                 return "-1";
             }
             return "0";
           }) |
           try_with([&item, &state, &path](const auto &direction) {
             auto maybe_fullname = rttt::try_fullname(item);
             logger::log_ifnot(maybe_fullname);
             auto parameters = cpr::Parameters{{"dir", direction},
                                               {"id", maybe_fullname.value()}};
             state.wait_list.push_back(
                 {cpr::PostAsync(cpr::Url{"https://oauth.reddit.com/api/vote"},
                                 state.credentials.header,
                                 cpr::Bearer{state.credentials.access_token},
                                 parameters, cpr::Timeout{5000}),
                  [name = path.name]([[maybe_unused]] cpr::Response &&req) {
                    // FIXME: Handle errors
                    if (req.status_code == 200) {
                      reddit::items.mark_for_update(name);
                      reddit::items.mark_active(name);
                    }
                  }});
             return true;
           }) |
           with([](auto maybe) { return maybe.value_or(false); });
  }
  if (event == ftxui::Event::Character('u')) {
    state.credentials.refresh_token = "";
    state = reddit::retrieve_refresh_token(std::move(state));
    ui_state.popup = create_login_popup(state, ui_state);
    return true;
  }
  return false;
}

Path parse_path(std::string_view name) {
  auto path = rttt::parse_path(name);
  return path;
}

auto setup([[maybe_unused]] state &state,
           [[maybe_unused]] nlohmann::json config) {
  state = rttt::reddit::retrieve_access_token(std::move(state), config);
}

auto retrieve_path_view([[maybe_unused]] state &state, const rttt::Path &path) {
  logger::log_ifnot(is_valid_path(path));
  // FIXME: items should be stored in the state
  if (!items.contains(path.name)) {
    items.insert({path.name, {}});
  }
  items.mark_active(path.name);
}

auto story_view([[maybe_unused]] state &state, const rttt::Path &path) {
  items.mark_active(path.name);
  return items.at(path.name) | std::views::transform([](const auto &item) {
           return std::pair(0, item);
         });
}

struct dig_helper {
  state *state_ptr = nullptr;
  rttt::active_storage<std::string, view::item_state> *item_view_states_ptr =
      nullptr;

  std::vector<ItemVariant> *operator()(auto &item) {
    logger::log_ifnot(state_ptr != nullptr);
    logger::log_ifnot(item_view_states_ptr != nullptr);
    auto maybe_id = try_id_string(item);
    if (!maybe_id.has_value() ||
        view::is_collapsed((*item_view_states_ptr), maybe_id.value()))
      return nullptr;

    if (std::holds_alternative<reddit::Story>(item)) {
      auto &ref = std::get<reddit::Story>(item);
      return &ref.kids;
    }
    logger::log_ifnot(std::holds_alternative<reddit::Comment>(item));
    return &std::get<reddit::Comment>(item).kids;
  }
};

auto comment_view(
    state &state, const rttt::Path &path,
    rttt::active_storage<std::string, view::item_state> &item_view_states) {
  dig_helper dig_f;
  dig_f.state_ptr = &state;
  dig_f.item_view_states_ptr = &item_view_states;

  return rttt::flat_view_with_depth(items.at(path.name), dig_f);
}

auto try_path_view(
    state &state, const rttt::Path &path,
    rttt::active_storage<std::string, view::item_state> &item_view_states) {
  logger::log_ifnot(is_valid_path(path));
  std::optional<
      std::variant<decltype(story_view(state, path)),
                   decltype(comment_view(state, path, item_view_states))>>
      opt(std::nullopt);
  if (path.parts.size() == 2) {
    if (items.contains(path.name)) {
      auto view = story_view(state, path);
      if (!view.empty())
        opt = view;
    }
  } else {
    if (items.contains(path.name) && !items.at(path.name).empty()) {
      opt = comment_view(state, path, item_view_states);
    }
  }
  return opt;
}

} // namespace reddit
} // namespace rttt
