#pragma once

#include <iostream>
#include <string>

#include "cpr/cpr.h"
#include "nlohmann/json.hpp"

#include "future.hpp"
#include "rttt.hpp"
#include "rttt/item.hpp"
#include "rttt/logger.hpp"
#include "rttt/text.hpp"
#include "rttt/view.hpp"
#include "storage.hpp"

namespace rttt {
namespace twitter {
struct state {
  future::future_wait_list<cpr::Response> priority_wait_list;
  future::future_wait_list<cpr::Response> wait_list;

  std::optional<std::string> access_token = std::nullopt;

  std::map<std::string, std::string> user_by_id;
  // Should not need updates and should live forever
  active_storage<std::string, std::string> user_ids =
      active_storage<std::string, std::string>(0, 5, 0);

  // Should we update individual tweets (using on_timeout)
  active_storage<std::string, tweet> tweets =
      active_storage<std::string, tweet>(600, 10, 1000);

  active_storage<std::string, std::vector<std::string>> tweets_by_user_id =
      active_storage<std::string, std::vector<std::string>>(600, 10, 1000);

  active_storage<std::string, std::vector<std::string>>
      tweets_by_conversation_id =
          active_storage<std::string, std::vector<std::string>>(600, 10, 1000);
};

// TODO: consider redesigning the API
// function that returns the pair below, e.g. future_get_bearer_token();
// state.wait_list.push_back(future_get_bearer_token(...));
void retrieve_bearer_token(state &state, const std::string &api_key,
                           const std::string &api_secret_key) {
  auto payload = cpr::Payload{{"grant_type", "client_credentials"}};
  auto auth = cpr::Authentication{api_key, api_secret_key
#if (CPR_VERSION_MAJOR > 1 || CPR_VERSION_MINOR > 8)
        ,
        cpr::AuthMode::BASIC
#endif
  };

  state.priority_wait_list.push_back(
      {cpr::PostAsync(cpr::Url{"https://api.twitter.com/oauth2/token"}, payload,
                      auth, cpr::Timeout{10000}),
       [&state, api_key, api_secret_key](cpr::Response &&response) {
         if (response.status_code != 200) {
           logger::push("TW: Retrying for access token");
           retrieve_bearer_token(state, api_key, api_secret_key);
           return;
         }
         logger::push("TW: Received access token");
         auto json = nlohmann::json::parse(response.text);
         state.access_token = json["access_token"].get<std::string>();
       }});
}

std::optional<std::string> try_user_id(const state &state,
                                       const std::string &name) {
  if (state.user_ids.contains(name)) {
    auto id = state.user_ids.at(name);
    if (!id.empty())
      return id;
  }
  return std::nullopt;
}

void retrieve_user_id(state &state, const std::string &username) {
  if (!state.user_ids.contains(username))
    state.user_ids.insert({username, ""});
  state.user_ids.mark_active(username);
}

tweet parse_tweet(const nlohmann::json &json, const std::string &username) {
  tweet tw;
  auto strings = rttt::text::split(json["text"].get<std::string>(), "\n", 2);
  tw.title = strings[0];
  if (strings.size() == 2)
    tw.text = strings[1];
  tw.id = json["id"];
  tw.descendants = json["public_metrics"]["reply_count"];
  tw.score = json["public_metrics"]["like_count"];
  tw.by = username;
  auto time_tm = parse_time(json["created_at"]);
  tw.time = std::mktime(&time_tm);
  return tw;
}

auto parse_tweets_by_user_id(state &state, const std::string &user_id,
                             const nlohmann::json &json) {
  // Add referenced users
  for (auto &&user : json["includes"]["users"]) {
    if (!state.user_ids.contains(user["id"])) {
      state.user_ids.insert({user["username"], user["id"]});
      state.user_ids.mark_updated(user["username"]);
      state.user_by_id[user["id"]] = user["username"];
    }
  }

  std::vector<std::string> ids;
  if (json.contains("data")) {
    for (auto &&tweet_json : json["data"]) {
      logger::log_ifnot(state.user_by_id.contains(tweet_json["author_id"]));
      auto tweet =
          parse_tweet(tweet_json, state.user_by_id.at(tweet_json["author_id"]));
      state.tweets.insert({tweet.id, tweet});
      state.tweets.mark_updated(tweet.id);
      ids.push_back(tweet.id);
    }
  }
  if (state.tweets_by_user_id.contains(user_id)) {
    state.tweets_by_user_id.at(user_id) = ids;
  } else {
    state.tweets_by_user_id.insert({user_id, ids});
  }
  state.tweets_by_user_id.mark_updated(user_id);
}

auto parse_tweets_by_conversation_id(state &state,
                                     const std::string &conversation_id,
                                     const nlohmann::json &json) {
  // TODO: some of this code is essentially the same as parse_tweets_by_user_id.
  // Consider putting shared code into a separate function
  // Add referenced users
  if (json.contains("includes")) {
    for (auto &&user : json["includes"]["users"]) {
      if (!state.user_ids.contains(user["id"])) {
        state.user_ids.insert({user["username"], user["id"]});
        state.user_ids.mark_updated(user["username"]);
        state.user_by_id[user["id"]] = user["username"];
      }
    }
  }

  if (json.contains("data")) {
    for (auto tweet_json : json["data"]) {
      logger::log_ifnot(state.user_by_id.contains(tweet_json["author_id"]));
      auto tweet =
          parse_tweet(tweet_json, state.user_by_id.at(tweet_json["author_id"]));
      state.tweets.insert({tweet.id, tweet});
      state.tweets.mark_updated(tweet.id);
    }
  }

  if (json.contains("includes")) {
    logger::log_ifnot(json["includes"].contains("tweets"));
    for (auto tweet_json : json["includes"]["tweets"]) {
      if (state.user_by_id.contains(tweet_json["author_id"])) {
        auto tweet = parse_tweet(tweet_json,
                                 state.user_by_id.at(tweet_json["author_id"]));
        state.tweets.insert({tweet.id, tweet});
        state.tweets.mark_updated(tweet.id);
      }
    }
  }

  // Second sort out the hierarchy
  logger::log_ifnot(state.tweets.contains(conversation_id));
  if (json.contains("data")) {
    for (auto tweet_json : json["data"]) {
      std::string id = tweet_json["id"];
      for (auto &&ref : tweet_json["referenced_tweets"]) {
        logger::log_ifnot(ref["type"] == "replied_to");
        if (state.tweets.contains(ref["id"])) {
          state.tweets.at(ref["id"]).kids.push_back(id);
        }
      }
    }
  }
  // Add the default api tweet
  state.tweets.at(conversation_id).kids.push_back("api7day");

  if (state.tweets_by_conversation_id.contains(conversation_id)) {
    state.tweets_by_conversation_id.at(conversation_id) = {conversation_id};
  } else {
    state.tweets_by_conversation_id.insert(
        {conversation_id, {conversation_id}});
  }
  state.tweets_by_conversation_id.mark_updated(conversation_id);
}

void retrieve_tweets_by_user_id(state &state, const std::string &user_id) {
  if (!state.access_token)
    return;

  auto parameters = cpr::Parameters{
      {"max_results", "100"},
      {"exclude", "replies,retweets"},
      {"expansions", "referenced_tweets.id,author_id"},
      {"user.fields", "name,username"},
      {"tweet.fields",
       "author_id,created_at,referenced_tweets,public_metrics"}};

  logger::push("TW: Requesting tweets for {}", user_id);
  state.wait_list.push_back(
      {cpr::GetAsync(
           cpr::Url{"https://api.twitter.com/2/users/" + user_id + "/tweets"},
           cpr::Bearer{state.access_token.value()}, parameters,
           cpr::Timeout{10000}),
       [&state, &user_id](cpr::Response &&response) {
         if (response.status_code != 200) {
           logger::push("TW: Unexpected status code {}", response.status_code);
           retrieve_tweets_by_user_id(state, user_id);
           return;
         }
         parse_tweets_by_user_id(state, user_id,
                                 nlohmann::json::parse(response.text));
       }});
}

auto update_story_view(state &state, const std::string &username) {
  if (!state.access_token)
    return;

  if (!state.tweets_by_user_id.on_timeout) {
    state.tweets_by_user_id.on_timeout =
        [&state](const std::string &key,
                 [[maybe_unused]] const std::vector<std::string> &value) {
          logger::log_ifnot(!key.empty());
          retrieve_tweets_by_user_id(state, key);
        };
  }
  auto maybe_id = twitter::try_user_id(state, username);
  if (!maybe_id.has_value()) {
    twitter::retrieve_user_id(state, username);
    return;
  }
  auto &id = maybe_id.value();
  if (state.tweets_by_user_id.contains(id)) {
    state.tweets_by_user_id.mark_active(id);
  } else {
    state.tweets_by_user_id.insert({id, {}});
  }
}

auto story_view(state &state, const std::string &username) {
  logger::log_ifnot(try_user_id(state, username).has_value());
  auto &user_id = state.user_ids.at(username);
  logger::log_ifnot(state.tweets_by_user_id.contains(user_id));
  return state.tweets_by_user_id.at(user_id) |
         std::views::filter([&state](const std::string &id) {
           return state.tweets.contains(id);
         }) |
         std::views::transform([&state](const std::string &id) {
           logger::log_ifnot(state.tweets.contains(id));
           state.tweets.mark_active(id);
           return std::pair{0, state.tweets.at(id)};
         });
}

struct dig_helper {
  state *state_ptr = nullptr;
  rttt::active_storage<std::string, view::item_state> *item_view_states_ptr =
      nullptr;
  std::vector<std::string> *operator()(const std::string &id) {
    logger::log_ifnot(state_ptr != nullptr);
    logger::log_ifnot(item_view_states_ptr != nullptr);
    if (!(*state_ptr).tweets.contains(id) ||
        view::is_collapsed(*item_view_states_ptr, id))
      return nullptr;
    return &((*state_ptr).tweets.at(id).kids);
  }
};

auto comment_view(
    state &state, const std::string &conversation_id,
    rttt::active_storage<std::string, view::item_state> &item_view_states) {
  logger::log_ifnot(state.tweets_by_conversation_id.contains(conversation_id));
  auto &ids = state.tweets_by_conversation_id.at(conversation_id);
  dig_helper dig_f;
  dig_f.state_ptr = &state;
  dig_f.item_view_states_ptr = &item_view_states;
  return rttt::flat_view_with_depth(ids, dig_f) |
         std::views::filter([&state](const auto &pair) {
           return state.tweets.contains(pair.second);
         }) |
         std::views::transform([&state](auto pair) {
           logger::log_ifnot(state.tweets.contains(pair.second));
           return std::pair(pair.first, state.tweets.at(pair.second));
         });
}

auto setup(state &state, nlohmann::json config) {
  // TODO: Add needed keys to the config here and save it, not in
  // config::load()?
  twitter::retrieve_bearer_token(
      state, config["twitter"]["api_key"].get<std::string>(),
      config["twitter"]["api_secret_key"].get<std::string>());
  twitter::tweet apitweet;
  apitweet.title = "Due to twitter API limitations we can only display "
                   "conversations less than 7 days old";
  apitweet.id = "api7day";
  apitweet.by = "rttt";
  state.tweets.insert({"api7day", std::move(apitweet)});

  state.user_ids.on_timeout = [&state](const std::string &key,
                                       std::string &value) {
    logger::push("TW: Requesting user id for {}", key);
    state.wait_list.push_back(
        {cpr::GetAsync(
             cpr::Url{"https://api.twitter.com/2/users/by/username/" + key},
             cpr::Bearer{state.access_token.value()}, cpr::Timeout{10000}),
         [&map = state.user_ids, &user_by_id = state.user_by_id, &key,
          &value](cpr::Response &&response) {
           if (response.status_code != 200) {
             logger::push("TW: Unexpected status code {}", response.status_code);
             map.mark_for_update(key);
             map.mark_active(key);
             return;
           }
           auto json = nlohmann::json::parse(response.text);
           value = json["data"]["id"];
           user_by_id[value] = key;
           map.mark_updated(key);
         }});
  };

  state.tweets_by_conversation_id.on_timeout =
      [&state](const std::string &key,
               [[maybe_unused]] std::vector<std::string> &value) {
        logger::push("TW: Requesting comments for {}", key);
        auto parameters = cpr::Parameters{
            {"query", "conversation_id:" + key},
            {"max_results", "100"},
            {"expansions", "referenced_tweets.id,author_id"},
            {"user.fields", "name,username"},
            {"tweet.fields",
             "author_id,created_at,referenced_tweets,public_metrics"}};

        state.wait_list.push_back(
            {cpr::GetAsync(
                 cpr::Url{"https://api.twitter.com/2/tweets/search/recent"},
                 cpr::Bearer{state.access_token.value()}, parameters,
                 cpr::Timeout{10000}),
             [&state, &key](cpr::Response &&response) {
               if (response.status_code != 200) {
                 logger::push("TW: Unexpected status code {}", response.status_code);
                 state.tweets_by_conversation_id.mark_for_update(key);
                 state.tweets_by_conversation_id.mark_active(key);
                 return;
               }

               twitter::parse_tweets_by_conversation_id(
                   state, key, nlohmann::json::parse(response.text));
               state.tweets_by_conversation_id.mark_updated(key);
             }});
      };
}

auto update(state &state) {
  bool updated = false;
  updated = updated || future::update(state.priority_wait_list);
  if (!state.access_token)
    return updated;

  updated = updated || future::update(state.wait_list);
  state.user_ids.update();
  state.tweets_by_user_id.update();
  state.tweets_by_conversation_id.update();
  state.tweets.update();
  return updated;
}

bool is_valid_path(std::string_view name) {
  // TODO: Ideally we should not split it twice (here and in parse_path)
  auto v = rttt::text::split(name, "/");
  return (v.size() > 0 && v[0] == "tw");
}

bool is_valid_path(const rttt::Path &path) {
  // TODO: should we rely on path.type if it is present?
  return (path.parts.size() == 2 || path.parts.size() == 4) &&
         path.parts[0] == "tw";
}

Path parse_path(std::string_view name) {
  auto path = rttt::parse_path(name);
  return path;
}

auto retrieve_story_view(state &state, const rttt::Path &path) {
  update_story_view(state, path.parts[1]);
}

auto try_story_view(state &state, const rttt::Path &path) {
  logger::log_ifnot(path.parts.size() > 1 && !path.parts[1].empty());
  auto &username = path.parts[1];
  std::optional<decltype(twitter::story_view(state, username))> opt(
      std::nullopt);
  auto maybe_user_id = twitter::try_user_id(state, username);
  if (!maybe_user_id.has_value() ||
      !state.tweets_by_user_id.contains(maybe_user_id.value()))
    return opt;

  opt = twitter::story_view(state, username);
  return opt;
}

auto retrieve_comment_view(state &state, const rttt::Path &path) {
  logger::log_ifnot(path.parts.size() > 3 && !path.parts[3].empty());
  if (!state.access_token)
    return;

  if (!state.tweets_by_conversation_id.contains(path.parts[3])) {
    state.tweets_by_conversation_id.insert({path.parts[3], {}});
  }
  state.tweets_by_conversation_id.mark_active(path.parts[3]);
}

auto try_comment_view(
    state &state, const rttt::Path &path,
    rttt::active_storage<std::string, view::item_state> &item_view_states) {
  logger::log_ifnot(path.parts.size() > 3 && !path.parts[3].empty());
  auto &conversation_id = path.parts[3];
  std::optional<decltype(twitter::comment_view(state, conversation_id,
                                               item_view_states))>
      opt(std::nullopt);
  if (!state.tweets_by_conversation_id.contains(conversation_id)) {
    return opt;
  }

  opt = twitter::comment_view(state, conversation_id, item_view_states);
  return opt;
}

auto retrieve_path_view(state &state, const rttt::Path &path) {
  logger::log_ifnot(twitter::is_valid_path(path));
  if (path.parts.size() == 2) {
    twitter::retrieve_story_view(state, path);
    return;
  }
  twitter::retrieve_comment_view(state, path);
}

auto try_path_view(
    state &state, const rttt::Path &path,
    rttt::active_storage<std::string, view::item_state> &item_view_states) {
  logger::log_ifnot(twitter::is_valid_path(path));
  std::optional<std::variant<decltype(twitter::story_view(state, {})),
                             decltype(twitter::comment_view(state, {},
                                                            item_view_states))>>
      opt(std::nullopt);
  if (path.parts.size() == 4) {
    auto maybe = twitter::try_comment_view(state, path, item_view_states);
    if (maybe.has_value()) {
      opt = maybe.value();
      return opt;
    }
  } else {
    auto maybe = twitter::try_story_view(state, path);
    if (maybe.has_value()) {
      opt = maybe.value();
      return opt;
    }
  }
  return opt;
}
} // namespace twitter
} // namespace rttt
