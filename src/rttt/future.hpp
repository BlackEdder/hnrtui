#pragma once

#include <future>
#include <list>

#include "rttt/logger.hpp"

namespace rttt {
namespace future {

template <typename T>
using future_wait_list =
    std::list<std::pair<std::future<T>, std::function<void(T &&)>>>;

template <typename T> auto update(future_wait_list<T> &list) {
  bool updated = false;
  auto it = list.begin();
  while (it != list.end()) {
    logger::log_ifnot(it->first.valid());
    auto status = it->first.wait_for(std::chrono::seconds(0));
    if (status == std::future_status::ready) {
      it->second(std::move(it->first.get()));
      it = list.erase(it);
      updated = true;
    } else {
      ++it;
    }
  }
  return updated;
}

} // namespace future
} // namespace rttt
