#include <chrono>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include <zmq.hpp>

#include "ftxui/component/captured_mouse.hpp"
#include "ftxui/component/component.hpp"
#include "ftxui/component/screen_interactive.hpp"
#include "ftxui/dom/elements.hpp"
#include "ftxui/dom/flexbox_config.hpp"

#include "rttt.hpp"
#include "rttt/config.hpp"
#include "rttt/fake_thing.hpp"
#include "rttt/functional.hpp"
#include "rttt/logger.hpp"
#include "rttt/prompt.hpp"
#include "rttt/text.hpp"
#include "rttt/thing.hpp"
#include "rttt/ui_ftxui.hpp"

/*
- [ ] Selecting text with the mouse in fxtui
*/

// helper functions
namespace {

std::map<std::string, std::string> parse_arguments(int argc, char **argv) {
  int last = argc;
  std::map<std::string, std::string> res;
  for (int i = 1; i < last; ++i) {
    res[argv[i]] = "";
    if (argv[i][0] == '-') {
      if (strlen(argv[i]) > 1) {
        res[std::string(1, argv[i][1])] =
            strlen(argv[i]) > 2 ? argv[i] + 2 : "";
      }
    }
  }

  return res;
}

rttt::active_storage<std::string, rttt::ui::scroll_window_state>
    window_state_cache =
        rttt::active_storage<std::string, rttt::ui::scroll_window_state>(0, 0,
                                                                         3500);

auto switch_path(rttt::Path &path,
                 rttt::ui::scroll_window_state &window_state) {
  return try_with([&path, &window_state](const rttt::Path &to) {
    if (window_state_cache.contains(path.name)) {
      window_state_cache.at(path.name) = window_state;
      window_state_cache.mark_active(path.name);
    } else {
      window_state_cache.insert({path.name, window_state});
    }
    if (!window_state_cache.contains(to.name)) {
      window_state_cache.insert({to.name, rttt::ui::scroll_window_state()});
      window_state_cache.at(to.name).layout =
          rttt::thing::default_view_mode(to);
    }
    window_state_cache.mark_active(to.name);
    window_state = window_state_cache.at(to.name);
    path = to;
    return 0;
  });
}
} // namespace

rttt::ui::state ui_state;
rttt::prompt::state prompt_state;
rttt::thing::state thing_state;

int main([[maybe_unused]] int argc, [[maybe_unused]] char **argv) {
  auto argm = parse_arguments(argc, argv);
  if (argm.find("--help") != argm.end() || argm.find("-h") != argm.end()) {
    printf("Usage: rttt [-h]\n");
    printf("    -h, --help  : print this help\n");
    printf("    --log       : save messages to log.txt\n");
    return -1;
  }

  if (argm.find("--log") != argm.end()) {
    logger::to_file("log.txt");
  }

  // state setup
  auto config = rttt::config::load();
  rttt::thing::setup(thing_state, config);
  rttt::thing::setup_commands(thing_state, ui_state);
  auto maybe_path =
      rttt::thing::parse_path(config["start_path"].get<std::string>());
  if (!maybe_path) {
    std::cerr << fmt::format("Failed parsing path {}", config["start_path"])
              << std::endl;
    return 1;
  }

  rttt::Path path = maybe_path.value();
  rttt::ui::scroll_window_state window_state;

  thing_state.commands |
      rttt::command::push(
          "open path",
          rttt::command::command() |
              rttt::command::function([&path,
                                       &window_state](std::string_view to) {
                rttt::thing::parse_path(to) | switch_path(path, window_state);
                return true;
              }) |
              rttt::command::help(" <path>: Open the given path, e.g. /rss"));

  prompt_state = prompt_state |
                 rttt::config::load_prompt_state_or(prompt_state) |
                 rttt::thing::completions(thing_state);

  // Item view states
  rttt::active_storage<std::string, rttt::view::item_state> item_view_states =
      rttt::config::load_item_view_states_or(
          rttt::active_storage<std::string, rttt::view::item_state>(
              0, 10, 3 * 24 * 3601));

  // Drawing windows
  auto screen = ftxui::ScreenInteractive::Fullscreen();

  auto scroll_window_renderer = ftxui::Renderer([&] {
    using namespace ftxui;
    rttt::thing::retrieve_path_view(thing_state, path);
    return rttt::thing::draw_path_view(
        thing_state, window_state, path, item_view_states, screen.dimx(),
        scroll_window_height(ui_state, screen.dimy()));
  });

  // Do I really need a renderer?
  // TODO: Use maybe to only render this when needed!
  auto messages_renderer = ftxui::Maybe(
      ftxui::Renderer([&] {
        using namespace ftxui;
        Elements elements{};
        for (size_t i = 0; i < logger::size(); ++i) {
          if (i == logger::size() - 1)
            elements.push_back(paragraphAlignLeft(logger::at(i)) | focus |
                               bold);
          else
            elements.push_back(paragraphAlignLeft(logger::at(i)));
        }
        return vbox(
            {text("Messages: " + std::to_string(logger::size())) | inverted,
             vbox(elements) | yframe |
                 size(HEIGHT, EQUAL, ui_state.status_window_height - 1)});
      }),
      &ui_state.status_open);

  auto popup_renderer = ftxui::Maybe(
      ftxui::Renderer([&] {
        return ftxui::window(ftxui::text(ui_state.popup.value().title),
                             ui_state.popup.value().content) |
               ftxui::clear_under | ftxui::center;
      }),
      [&popup = ui_state.popup]() { return popup.has_value(); });

  // move these to prompt state
  std::string input = "/";
  ftxui::InputOption prompt_option;
  int prompt_position = 1;
  prompt_option.on_enter = [&]() {
    ui_state.prompt_open = false;
    prompt_state = rttt::prompt::push(std::move(prompt_state), input);
    if (input[0] == '/') {
      rttt::thing::parse_path(input) | switch_path(path, window_state);
    } else if (input[0] == ':') {
      thing_state | rttt::thing::execute_command(input);
    }
  };
  prompt_option.cursor_position = &prompt_position;
  auto prompt = ftxui::Maybe(ftxui::Input(&input, "/r/front", prompt_option),
                             &ui_state.prompt_open);

  auto main_container = ftxui::Container::Vertical(
      {scroll_window_renderer, messages_renderer, prompt});

  auto main_renderer = Renderer(main_container, [&] {
    auto header =
        ftxui::hbox(
            {ftxui::separatorEmpty(),
             ftxui::text(rttt::thing::window_header(thing_state, path)) |
                 ftxui::xflex_grow,
             ftxui::text("Press '?' for help"), ftxui::separatorEmpty()}) |
        ftxui::xflex_grow | ftxui::inverted;
    ftxui::Element document =
        ftxui::vbox({header, scroll_window_renderer->Render(),
                     messages_renderer->Render(), prompt->Render()});
    if (ui_state.popup.has_value()) {
      document = ftxui::dbox({document | ftxui::dim, popup_renderer->Render()});
    }
    return document;
  });

  auto component = CatchEvent(main_renderer, [&](ftxui::Event event) {
    if (ui_state.popup.has_value()) {
      return ui_state.popup.value().event_handler(event);
    }
    if (ui_state.prompt_open) {
      if (event == ftxui::Event::Tab) {
        rttt::prompt::tab_completion(prompt_state, input, prompt_position);
        return true;
      } else if (event == ftxui::Event::Escape) {
        ui_state.prompt_open = false;
        return true;
      } else if (event == ftxui::Event::ArrowUp) {
        rttt::prompt::arrow(prompt_state, input, true);
        return true;
      } else if (event == ftxui::Event::ArrowDown) {
        rttt::prompt::arrow(prompt_state, input, false);
        return true;
      }
      return false;
    }
    if (rttt::thing::catch_ui_event(thing_state, ui_state, path,
                                    window_state.highlighted_item, event)) {
      return true;
    }
    if (event == ftxui::Event::Character('q')) {
      screen.ExitLoopClosure()();
      return true;
    } else if (event == ftxui::Event::Character('k')) {
      window_state.goto_prev_item = true;
      return true;
    } else if (event == ftxui::Event::Character('j')) {
      window_state.goto_next_item = true;
      return true;
    } else if (event == ftxui::Event::Character('K')) {
      auto maybe_id = rttt::try_id_string(window_state.highlighted_item);
      if (maybe_id.has_value()) {
        item_view_states = rttt::view::toggle_read(std::move(item_view_states),
                                                   maybe_id.value());
        window_state.goto_prev_item = true;
      }
      return true;
    } else if (event == ftxui::Event::Character('J')) {
      auto maybe_id = rttt::try_id_string(window_state.highlighted_item);
      if (maybe_id.has_value()) {
        item_view_states = rttt::view::toggle_read(std::move(item_view_states),
                                                   maybe_id.value());
        window_state.goto_next_item = true;
      }
      return true;
    } else if (event == ftxui::Event::Character(' ')) {
      auto maybe_id = rttt::try_id_string(window_state.highlighted_item);
      if (maybe_id.has_value()) {
        item_view_states = rttt::view::toggle_collapsed(
            std::move(item_view_states), maybe_id.value());
      }
      return true;
    } else if (event == ftxui::Event::Character('o')) {
      auto maybe_url = try_url(window_state.highlighted_item);
      if (maybe_url.has_value() &&
          window_state.layout != rttt::view::layout::text) {
        rttt::openInBrowser(maybe_url.value());
        return true;
      }
      std::vector<std::string> popup_urls = {rttt::thing::path_url(path)};
      if (maybe_url)
        popup_urls.push_back(maybe_url.value());
      auto maybe_title = try_title(window_state.highlighted_item);
      if (maybe_title) {
        auto u = rttt::extractURL(maybe_title.value());
        popup_urls.insert(popup_urls.end(), u.begin(), u.end());
      }
      auto maybe_text = try_text(window_state.highlighted_item);
      if (maybe_text) {
        auto u = rttt::extractURL(maybe_text.value());
        popup_urls.insert(popup_urls.end(), u.begin(), u.end());
      }
      ui_state.popup = rttt::ui::create_url_popup(ui_state, popup_urls);
      popup_renderer->TakeFocus();
      return true;
    } else if (event == ftxui::Event::Character('g')) {
      window_state.goto_top = true;
      return true;
    } else if (event == ftxui::Event::Character('l')) {
      auto from = path;
      rttt::thing::focus_path(std::move(from), window_state.highlighted_item) |
          switch_path(path, window_state);
      return true;
    } else if (event == ftxui::Event::Character('h')) {
      auto from = path;
      // Mark read when returning
      item_view_states =
          rttt::view::mark_read(std::move(item_view_states), from.id);

      rttt::thing::unfocus_path(std::move(from)) |
          switch_path(path, window_state);
      return true;
    } else if (event == ftxui::Event::Character('v')) {
      if (window_state.layout == rttt::view::layout::text)
        window_state.layout ^= rttt::view::layout::text;
      else
        window_state.layout |= rttt::view::layout::text;
      return true;
    } else if (event == ftxui::Event::Character('?')) {
      ui_state.popup = rttt::ui::create_help_popup(ui_state);
      popup_renderer->TakeFocus();
      return true;
    } else if (event == ftxui::Event::Character('s')) {
      ui_state.status_open = !ui_state.status_open;
      return true;
    } else if (event == ftxui::Event::Character('/')) {
      input = "/";
      prompt_position = 1;
      ui_state.prompt_open = true;
      prompt->TakeFocus();
      return true;
    } else if (event == ftxui::Event::Character(':')) {
      input = ":";
      prompt_position = 1;
      ui_state.prompt_open = true;
      prompt->TakeFocus();
      return true;
    }
    return false;
  });

  zmq::context_t ctx;
  zmq::socket_t sock(ctx, zmq::socket_type::pull);
  sock.bind("ipc:///tmp/rttt.ipc");

  bool refresh_ui_continue = true;
  std::thread refresh_ui([&] {
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(0.05s);
    // This ensures that the screen will be initialized properly
    screen.PostEvent(ftxui::Event::Custom);
    size_t counter = 0;
    while (refresh_ui_continue) {
      // This should probably use screen.Post(Task) for thread safety
      std::this_thread::sleep_for(0.05s);
      if (rttt::thing::update(thing_state)) // Redraw
        screen.PostEvent(ftxui::Event::Custom);
      if (logger::size() > 100)
        logger::pop_front();
      if (counter % 10000 == 0) {
        item_view_states.update();
        window_state_cache.update();
        rttt::config::save_item_view_states(item_view_states);
        rttt::config::save_prompt_state(prompt_state);
      }

      // rttt-send msg
      zmq::message_t msg;
      auto result = sock.recv(msg, zmq::recv_flags::dontwait);
      result | try_with([&]([[maybe_unused]] const auto &value) {
        thing_state.commands | rttt::command::execute(msg.to_string());
        logger::push("rttt-send: {}", msg.str());
        screen.PostEvent(ftxui::Event::Custom);
        return 0;
      });
      ++counter;
    }
  });

  screen.Loop(component);
  refresh_ui_continue = false;

  rttt::config::save_item_view_states(item_view_states);
  rttt::config::save_prompt_state(prompt_state);

  refresh_ui.join();
  return EXIT_SUCCESS;
}
