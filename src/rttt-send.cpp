#include <iostream>

#include "fmt/format.h"
#include <zmq.hpp>

int main([[maybe_unused]] int argc, [[maybe_unused]] char **argv) {
  std::string message = ":log Hello from rttt-send";
  if (argc > 1)
    message = std::string(argv[1]);

  std::cout << fmt::format("Sending message: {}", message) << std::endl;

  zmq::context_t ctx;
  zmq::socket_t sock(ctx, zmq::socket_type::push);
  sock.connect("ipc:///tmp/rttt.ipc");
  sock.send(zmq::buffer(message), zmq::send_flags::dontwait);
}
