#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "rttt/future.hpp"

using namespace rttt;

SCENARIO("We can insert an async task and execute it on the background") {
  std::string result = "";
  future::future_wait_list<std::string> wait_list;

  std::future<std::string> future = std::async(std::launch::async, []() {
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    return std::string("bla1");
  });

  wait_list.push_back(
      {std::move(future), [&result](std::string &&str) { result = str; }});

  REQUIRE(wait_list.size() == 1);

  size_t i = 0;

  while (result.empty()) {
    future::update(wait_list);
    ++i;
  }
  REQUIRE(result == "bla1");
  REQUIRE(wait_list.size() == 0);

  // Make sure it didn't block the while loop
  REQUIRE(i > 1);
}
