#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "cpr/cpr.h"
#include "nlohmann/json.hpp"

#include "rttt.hpp"
#include "rttt/reddit.hpp"

using namespace rttt;

SCENARIO("We can parse a path") {
  auto front_path = rttt::parse_path("/r/front");
  auto neovim_path = rttt::parse_path("/r/neovim");
  auto front_with_comment_path = rttt::parse_path("/r/front/comments/article");
  auto neovim_with_comment_path = rttt::parse_path("/r/neovim/comments/article");
  REQUIRE(reddit::getURIFromPath(front_path) == "https://oauth.reddit.com/best");
  REQUIRE(front_path.name == "/r/front");
  REQUIRE(front_with_comment_path.name == "/r/front/comments/article");
  REQUIRE(reddit::getURIFromPath(neovim_path) ==
          "https://oauth.reddit.com/r/neovim/best");
  REQUIRE(reddit::getURIFromPath(front_with_comment_path) == "https://oauth.reddit.com/comments/article");
  REQUIRE(reddit::getURIFromPath(neovim_with_comment_path) ==
          "https://oauth.reddit.com/r/neovim/comments/article");

  rttt::Path p = rttt::parse_path("/r/front");
  REQUIRE(reddit::getURIFromPath(p) == "https://oauth.reddit.com/best");
  p = rttt::parse_path("/r/front/comments/article");
  REQUIRE(reddit::getURIFromPath(p) ==
          "https://oauth.reddit.com/comments/article");

  p = rttt::parse_path("/r/neovim");
  REQUIRE(reddit::getURIFromPath(p) == "https://oauth.reddit.com/r/neovim/best");
  p = rttt::parse_path("/r/neovim/comments/article");
  REQUIRE(reddit::getURIFromPath(p) ==
          "https://oauth.reddit.com/r/neovim/comments/article");
}

SCENARIO("We can parse a json listing") {
  // load test/reddit_listing.json
  FILE *pFile;

  pFile = fopen("test/reddit_listing.json", "r");
  // parse it to json
  auto j = nlohmann::json::parse(pFile);
  REQUIRE(!j.is_array());
  // parse it to items
  auto items = reddit::parseListing(j);
  // check length and some other stuff
  REQUIRE(items.size() == 2);

  // Comments, note that here body holds the text
  pFile = fopen("test/reddit_comments.json", "r");
  // parse it to json
  j = nlohmann::json::parse(pFile);
  REQUIRE(j.is_array());
  items = reddit::parseComments(j);
  REQUIRE(items.size() == 1);

  auto story = std::get<reddit::Story>(items[0]);
  REQUIRE(story.kids.size() == 3);
}

SCENARIO("We can flatten subreddits") {
  FILE *pFile;

  pFile = fopen("test/reddit_comments.json", "r");
  // parse it to json
  auto j = nlohmann::json::parse(pFile);
  REQUIRE(j.is_array());
  auto items = reddit::parseComments(j);
  REQUIRE(items.size() == 1);
  auto story = std::get<reddit::Story>(items[0]);
  REQUIRE(story.kids.size() == 3);

  constexpr auto dig_function = [](auto &item) -> auto {
    if (std::holds_alternative<reddit::Story>(item))
      return &std::get<reddit::Story>(item).kids;
    assert(std::holds_alternative<reddit::Comment>(item));
    return &std::get<reddit::Comment>(item).kids;
  };
  auto view = flat_view_with_depth(items, dig_function);
  auto it = view.begin();
  REQUIRE((*it).first == 0);
  REQUIRE(try_id_string((*it).second).value() == "rsszvq");
  ++it;
  REQUIRE((*it).first == 1);
  REQUIRE(try_id_string((*it).second).value() == "hqobgse");
  ++it;
  REQUIRE((*it).first == 1);
  REQUIRE(try_id_string((*it).second).value() == "hqohd2n");
  ++it;
  REQUIRE((*it).first == 2);
  REQUIRE(try_id_string((*it).second).value() == "hqop8kv");
  REQUIRE((it != view.end()));
  ++it;
  REQUIRE((*it).first == 3);
  REQUIRE(try_id_string((*it).second).value() == "hqoynyq");
  ++it;
  REQUIRE((*it).first == 2);
  REQUIRE(try_id_string((*it).second).value() == "hqoxazw");
  --it;
  REQUIRE((*it).first == 3);
  REQUIRE(try_id_string((*it).second).value() == "hqoynyq");
}



SCENARIO("We can extract code from a string") {
  std::string str = "GET /?state=abcde&code=och5-csNgMKnOB6hLSVqOILJk9uqzw HTTP/1.1";
  auto maybe_code = reddit::try_to_extract_code(str);
  REQUIRE(maybe_code);
  REQUIRE(maybe_code.value() == "och5-csNgMKnOB6hLSVqOILJk9uqzw"); 
}

SCENARIO("We can parse user info for the username") {
  FILE *pFile;

  pFile = fopen("test/api_v1_me.json", "r");
  // parse it to json
  auto j = nlohmann::json::parse(pFile);
  REQUIRE(j["name"] == "BlackEdder");
}

SCENARIO("We can parse subreddits") {
  FILE *pFile;

  pFile = fopen("test/subreddits.json", "r");
  // parse it to json
  auto j = nlohmann::json::parse(pFile);
  for (auto &child : j["data"]["children"]) {
    auto nm = "/" + child["data"]["display_name_prefixed"].get<std::string>();
    REQUIRE(nm.substr(0, 3) == "/r/");
  }
}


