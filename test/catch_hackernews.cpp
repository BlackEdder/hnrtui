#define CATCH_CONFIG_MAIN

#include <ranges>

#include "catch2/catch.hpp"

#include "fmt/format.h"

#include "rttt/hackernews.hpp"
#include "rttt/item.hpp"

using namespace rttt;

hackernews::Story create_hackernews_story(std::string title, size_t id) {
  auto item = hackernews::Story();
  item.title = title;
  item.id = id;
  return item;
}

template <typename T> void print_path_view(T &variant) {
  std::visit(
      [](auto &v) {
        std::cout << "ITEMS" << std::endl;
        for (auto pair : v | std::views::take(5)) {

          std::cout << "depth: " << pair.first << std::endl;
          auto maybe_title = try_title(pair.second);
          if (maybe_title.has_value())
            std::cout << "Title: " << maybe_title.value() << std::endl;
          else
            std::cout << "Title: NONE" << std::endl;

          auto maybe_text = try_text(pair.second);
          if (maybe_text.has_value())
            std::cout << "Text: " << maybe_text.value() << std::endl;
          else
            std::cout << "Text: NONE" << std::endl;
        }
      },
      variant);
}

template <typename T> void walk_path_view(T &variant) {
  std::visit(
      [](auto &v) {
        for (auto pair : v | std::views::take(5)) {
          auto it = pair.first;
        }
      },
      variant);
}



SCENARIO("We can use the path API", "[.,api]") {
  hackernews::state state;
  hackernews::setup(state, config::defaultConfig());

  std::string pth = "/hn/top";
  REQUIRE(hackernews::is_valid_path(pth));

  auto path = hackernews::parse_path(pth);
  REQUIRE(path.parts[1] == "top");
  size_t count = 5000;

  rttt::active_storage<std::string, view::item_state> dummy;

  REQUIRE(!hackernews::try_path_view(state, path, dummy).has_value());
  while (
    !hackernews::try_path_view(state, path, dummy).has_value() &&
         count > 0) {
    hackernews::retrieve_path_view(state, path);
    hackernews::update(state);
    --count;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  REQUIRE(count > 0);
  REQUIRE(hackernews::try_path_view(state, path, dummy).has_value());
  auto variant = hackernews::try_path_view(state, path, dummy).value();
  //print_path_view(variant);
  std::visit(
      [](auto &v) {
        REQUIRE(!v.empty());
      },
      variant);
  std::string c_id;
  std::visit(
      [&c_id](auto &v) {
        auto maybe_id = rttt::try_id_string((*v.begin()).second);
        REQUIRE(maybe_id.has_value());
        c_id = maybe_id.value();
      },
      variant);
  REQUIRE(!c_id.empty());
  std::cout << fmt::format("c_id {}", c_id) << std::endl;
  pth += std::string("/comments/") + c_id;
  REQUIRE(hackernews::is_valid_path(pth));
  path = hackernews::parse_path(pth);
  REQUIRE(path.parts[1] == "top");
  REQUIRE(path.parts[3] == c_id);
  REQUIRE(path.mode == rttt::list_mode::comment);

  /* Do we have kids?
  */
  auto iv = state.items.at(std::stoi(c_id));
  std::visit([](const auto &item) {
               if constexpr (requires { item.kids; }) {
                 //std::string str = fmt::format("Kids: {}", fmt::join(item.kids, ","));
                 std::string str = fmt::format("No. of kids: {}", item.kids.size());
                 std::cout << str << std::endl;
               }
             }, iv);


  // Check that length is only 1 instead (i.e. first story) 
  //REQUIRE(!hackernews::try_path_view(state, path, dummy).has_value());

  count = 100;
  //TODO: count = 5000;
  while (//!hackernews::try_path_view(state, path, dummy).has_value() &&
         count > 0) {
    hackernews::retrieve_path_view(state, path);
    auto v = hackernews::try_path_view(state, path, dummy);
    // New items are only loaded/retrieved if we access them
    if (v.has_value())
        walk_path_view(v.value());
    hackernews::update(state);
    --count;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    
  }
  variant = hackernews::try_path_view(state, path, dummy).value();
  std::visit(
      [](auto &v) {
        REQUIRE(!v.empty());
      },
      variant);
  /*for (auto i = 0; i < logger::size(); ++i)
    std::cout << logger::at(i) << std::endl;*/
  //print_path_view(variant);
  //REQUIRE(count > 0);
}

