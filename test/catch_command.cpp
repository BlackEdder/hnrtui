#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "rttt/command.hpp"
using namespace rttt;

SCENARIO("Push new commands") {
  command::list commands;
  commands =
      commands |
      command::push("openurl", [](const std::string_view url) { return true; });

  REQUIRE(commands.size() == 1);

  commands.clear();

  commands =
      commands |
      command::push("openurl",
                    [](const std::string_view url) { return true; }) |
      command::push("quit", []() { return true; }) |
      command::push("help", [](const std::optional<std::string_view> &topic) {
        return true;
      });
  REQUIRE(commands.size() == 3);

  // Feels like this could lead to a segmentation fault, because command::list
  // only exists temporary, but it seems to work?
  auto subcommands =
      command::list() |
      command::push("feed", command::list() |
                                command::push("add", []() { return true; }));
  REQUIRE(subcommands.size() == 1);

  commands = commands | command::push("rss", subcommands);
  REQUIRE(commands.size() == 4);
}

auto setup_commands(size_t &id, std::string &arg) {
  command::list commands;
  commands =
      commands | command::push("openurl", [&](const std::string_view url) {
        id = 1;
        arg = url;
        return true;
      });

  REQUIRE(commands.size() == 1);

  commands.clear();

  commands =
      commands |
      command::push("openurl",
                    command::command() |
                        command::function([&](const std::string_view url) {
                          id = 1;
                          arg = url;
                          return true;
                        }) |
                        command::help(": Open an url")) |
      command::push("quit",
                    [&]() {
                      id = 2;
                      return true;
                    }) |
      command::push("help", [&](const std::optional<std::string_view> &topic) {
        id = 3;
        if (topic.has_value())
          arg = topic.value();
        else
          arg.clear();
        return true;
      });
  REQUIRE(commands.size() == 3);

  // Feels like this could lead to a segmentation fault, because command::list
  // only exists temporary, but it seems to work?
  auto subcommands =
      command::list() |
      command::push(
          "feed",
          command::list() |
              command::push(
                  "add", command::command() |
                             command::function([&](const std::string_view url) {
                               arg = url;
                               id = 4;
                               return true;
                             }) |
                             command::help(" <feed>: Add an rss feed")));
  REQUIRE(subcommands.size() == 1);

  commands = commands | command::push("rss", subcommands);
  REQUIRE(commands.size() == 4);
  return commands;
}

SCENARIO("Execute commands") {
  size_t id = 0;
  std::string arg = "";
  auto commands = setup_commands(id, arg);
  auto exe = commands | command::execute("openurl", "test");
  REQUIRE(exe);
  REQUIRE(arg == "test");
  exe = commands | command::execute("wrong", "test");
  REQUIRE(!exe);

  exe = commands | command::execute("quit");
  REQUIRE(exe);
  REQUIRE(id == 2);

  exe = commands | command::execute("rss", "feed add url");
  REQUIRE(exe);
  REQUIRE(arg == "url");
  REQUIRE(id == 4);

  // : is handled correctly
  exe = commands | command::execute(":quit");
  REQUIRE(exe);
  REQUIRE(id == 2);
  exe = commands | command::execute(":rss feed add url");
  REQUIRE(exe);
  REQUIRE(arg == "url");
  REQUIRE(id == 4);
}

SCENARIO("We can easily convert it to completions") {
  size_t id = 0;
  std::string arg = "";
  auto commands = setup_commands(id, arg);
  auto v = commands | command::as_completions();
  REQUIRE(v.size() == 6);
  for (auto &s : v)
    REQUIRE(s[0] == ':');
}

SCENARIO("We can get help messages") {
  size_t id = 0;
  std::string arg = "";
  auto commands = setup_commands(id, arg);
  auto v = commands | command::as_help();
  REQUIRE(v.size() == 2);
  for (auto &s : v)
    REQUIRE(s[0] == ':');

  // v = commands | command::filter("rss feed") | command::as_help();
  v = commands | command::as_help("rss feed");
  REQUIRE(v.size() == 1);
  for (auto &s : v)
    REQUIRE(s[0] == ':');
}

SCENARIO("We can add subcommands by full command name") {
  std::string str = "you should never see this";

  command::list commands;
  commands = commands |
             command::push("open cmda",
                           [&str](const std::string_view url) {
                             str = url;
                             return true;
                           }) |
             command::push("open cmdb with", [&str](const std::string_view url) {
               str = url;
               return true;
             })  |
             command::push("open cmdb noarg", [&str]() {
               str = "empty";
               return true;
             });
 
  REQUIRE(commands.size() == 1);

  bool exe = commands | command::execute(":open");
  REQUIRE(!exe);

  exe = commands | command::execute(":open cmda urla");
  REQUIRE(str == "urla");
  REQUIRE(exe);

  exe = commands | command::execute(":open cmdb with urlb");
  REQUIRE(str == "urlb");
  REQUIRE(exe);

  exe = commands | command::execute(":open cmdb noarg");
  REQUIRE(str == "empty");
  REQUIRE(exe);

  auto v = commands | command::as_completions();
  REQUIRE(v.size() == 5);
}
